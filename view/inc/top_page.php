<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>MASTERSPORT</title>
    <link href="view/css/style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <script type="text/javascript" src=view\Lang\translate.js></script>
    <script src="module/sport/model/validate_sport.js"></script>

    
</head>

<body>