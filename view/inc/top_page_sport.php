<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>Alta de Usuario</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.css" />
	<!-- dataTables -->
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<!-- dataTables -->
	<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

	<!-- -->
	<script type="text/javascript" src=view\Lang\translate.js></script>
	<script type="text/javascript">
		$(function() {
			$('#date_birthday').datepicker({
				dateFormat: 'dd/mm/yy',
				changeMonth: true,
				changeYear: true,
				yearRange: '1900:2020',
				onSelect: function(selectedDate) {}
			});
		});
	</script>
	<link href="view/css/style.css" rel="stylesheet" type="text/css" />
	<script src="module/sport/model/validate_sport.js"></script>
	<script src="module/sport/model/read_modal.js"></script>
</head>

<body>